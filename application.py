import flask
from flask import Flask
# bootstrap
from flask_bootstrap3 import Bootstrap
# flask login
from flask_login import login_required,current_user
# flask-navigation
from flask_navigation import Navigation
# database
from engines import Engine
# import blueprints
from mod_auth import mod_auth
from mod_dummy import mod_dummy


# create backend
backend=Engine.new(mock=False)

# create flask app
app = Flask(__name__)
app.config.update(
    SECRET_KEY='guess what'


)


# bootstrap
Bootstrap(app)

# register blueprints
app.register_blueprint(mod_auth)
app.register_blueprint(mod_dummy)

# register flask-navigation
nav = Navigation(app)

# create navbar
nav.Bar('top', [
    nav.Item('Home', 'index'),
    nav.Item('dummies', 'dummy.dummy_list'),
])





@app.route('/')
def index():

    user= current_user
    if current_user.is_anonymous:
        user.username= 'anonymous'

    return flask.render_template('index.html',user=user,header='home')



@app.route("/settings")
@login_required
def settings():
    """

    :return:
    """
    pass
    return "settings"


# @app.route("/navbar")
# def navbar():
#     """
#
#     :return:
#     """
#     user= current_user
#     if current_user.is_anonymous:
#         user.username= 'anonymous'
#
#
#     return flask.render_template('boot_base.html',user=user)



if __name__ == '__main__':
    app.debug=True
    app.run()

