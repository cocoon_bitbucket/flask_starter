import flask
from flask import Flask

# # flask wtf
# import flask_wtf as wtf
# import wtforms
# from wtforms.validators import DataRequired

from models import User , LoginForm

# bootstrap
from flask_bootstrap3 import Bootstrap

# flask login
from flask_login import LoginManager,login_required,login_user,logout_user,current_user



app = Flask(__name__)
app.config.update(
    SECRET_KEY='guess what'
)



# login manager
login_manager = LoginManager()
login_manager.init_app(app)


# bootstap
Bootstrap(app)



@login_manager.user_loader
def load_user(user_id):
    return User.get(user_id)


@app.route('/')
def index():

    user= current_user
    if current_user.is_anonymous:
        username= 'anonymous'
    else:
        username= current_user.username


    return 'welcome  %s!' % username


@app.route('/login', methods=['GET', 'POST'])
def login():
    # Here we use a class of some kind to represent and validate our
    # client-side form data. For example, WTForms is a library that will
    # handle this for us, and we use a custom LoginForm to validate.
    form = LoginForm()
    if form.validate_on_submit():
        # Login and validate the user.
        # user should be an instance of your `User` class
        user= User.get(form.username.data)

        login_user(user)

        # get session
        session = flask.globals.session
        token= session['csrf_token']
        session_id= session['_id']
        session_fresh= session['_fresh']
        session_user_id= session['user_id']
        session_modified= session.modified
        session_new= session.new
        session_permanent=session.permanent


        #session = LocalProxy(partial(_lookup_req_object, 'session'))


        flask.flash('Logged in successfully.')

        next = flask.request.args.get('next')
        # next_is_valid should check if the user has valid
        # permission to access the `next` url
        #if not next_is_valid(next):
        #    return flask.abort(400)

        return flask.redirect(next or flask.url_for('index'))
    return flask.render_template('signin.html', form=form)



@app.route("/logout")
@login_required
def logout():
    logout_user()
    return flask.redirect(flask.url_for('index'))



@app.route("/settings")
@login_required
def settings():
    """

    :return:
    """
    pass
    return "settings"






if __name__ == '__main__':
    app.debug=True
    app.run()

