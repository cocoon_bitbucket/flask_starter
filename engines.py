
import json
try:
    import redis
    redis_available = True
except ImportError:
    redis_available= None


import mockredis


REDIS_PWD_URL = "redis://:password@localhost:6379/0"
REDIS_URL = "redis://localhost:6379/0"



class Engine(object):
    """
        a redis engine db factory ( either redis or mock redis


        usage:

            # real redis
            db= Engine.from_url( "redis://localhost:6379/0")
            db= Engine.get_instance()


            # mockredis
            db= Engine.from_url( "redis://localhost:6379/0", mock=True)


    """
    _state={}

    @classmethod
    def get_engine(cls,engine='redis',strict=True):
        """
            build and return a RedisClass class

        :param engine:
        :return:
        """
        # select backend engine  native or redis
        if engine == 'redis':
            # real redis
            if not redis_available:
                raise RuntimeError('you need redis client: pip install redis')
            if strict:
                backend_engine= type('StrictRedisDb',(JsonRedis,redis.StrictRedis),{})
            else:
                backend_engine= type('RedisDb',(JsonRedis,redis.Redis),{})
        else:
            # mock redis
            backend_engine=  type('MockRedisDb',(JsonRedis, mockredis.MockRedis),{})

        return backend_engine

    @classmethod
    def new(cls, url=None, strict=True, name='default', mock=False):
        """

        :param url:
        :param strict:
        :param name:
        :param memory:
        :return:
        """
        if mock:
            # mockredis
            engine= cls.get_engine( 'native')
            db= engine()
        else:
            # real redis
            engine= cls.get_engine('redis',strict=strict)
            # default redis config if no url
            url = url or REDIS_URL
            db= engine.from_url(url)


        cls._state[name]=db
        return db


    @classmethod
    def from_mock(cls,name='default'):
        """

        :param name:
        :return:
        """
        # mockredis
        engine = cls.get_engine('native')
        db = engine()

        cls._state[name] = db
        return db



    @classmethod
    def get_instance(cls,name='default'):
        """

        :param name:
        :return:
        """
        return cls._state[name]


class JsonRedis(object):
    """
        a Redis client where get and set are json values
    """
    def set(self,name,value,**kwargs):
        """

        :param name:
        :param value:
        :return:
        """
        value=json.dumps(value)
        return super(JsonRedis,self).set(name,value,**kwargs)

    def get(self,name):
        """

        :param name:
        :return:
        """
        value= super(JsonRedis,self).get(name)
        if value is None :
            return None
        value= json.loads(value)
        return value



if __name__=="__main__":



    db= Engine.new()

    db2= Engine.get_instance()

    assert db == db2

    value = {'data': 'data'}
    db.set('k1',value)
    v= db.get('k1')

    assert v == value



    engine= Engine.get_engine()

    store = engine.from_url(REDIS_URL)


    value= {'data':'data'}
    store.set('k1',value)

    v= store.get('k1')

    assert v == value


    engine= Engine.get_engine('redis')

    store = engine.from_url(REDIS_URL)


    store.set('k1',value)

    v= store.get('k1')

    assert v == value







    print "Done."