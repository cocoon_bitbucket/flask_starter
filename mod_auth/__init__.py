
import base64
import flask
from flask import Blueprint

# flask login
from flask_login import LoginManager,login_required,login_user,logout_user,current_user

from models import User , LoginForm

from ldap_piqs import ldap_authent


login_manager = LoginManager()
mod_auth = Blueprint('auth', __name__, template_folder="templates")

#from views import *


@mod_auth.record_once
def on_load(state):
    login_manager.init_app(state.app)
    login_manager.login_view = 'auth.login'


@login_manager.user_loader
def load_user(user_id):
    return User.get(user_id)


@login_manager.request_loader
def load_user_from_request(request):

    # first, try to login using the api_key url arg
    api_key = request.args.get('api_key')
    if api_key:
        user = User.query.filter_by(api_key=api_key).first()
        if user:
            return user

    # next, try to login using Basic Auth
    api_key = request.headers.get('Authorization')
    if api_key:
        api_key = api_key.replace('Basic ', '', 1)
        try:
            api_key = base64.b64decode(api_key)
        except TypeError:
            pass
        user = User.query.filter_by(api_key=api_key).first()
        if user:
            return user

    # finally, return None if both methods did not login the user
    return None



@mod_auth.route('/login', methods=['GET', 'POST'])
def login():
    # Here we use a class of some kind to represent and validate our
    # client-side form data. For example, WTForms is a library that will
    # handle this for us, and we use a custom LoginForm to validate.
    form = LoginForm()
    if form.validate_on_submit():
        # Login and validate the user.
        # user should be an instance of your `User` class

        username= form.username.data
        password= form.password.data


        # insert here ldap
        auth= ldap_authent(username,password)

        if auth:
            # authent ok : create user
            user= User(username,{'name':username})
            user.save()
        else:
            # failed to log
            flask.flash('Login to ldap has failded...')
            return flask.redirect(flask.url_for("auth.login"))


        user= User.get(username)

        login_user(user)

        # get session
        session = flask.globals.session
        token= session['csrf_token']
        session_id= session['_id']
        session_fresh= session['_fresh']
        session_user_id= session['user_id']
        session_modified= session.modified
        session_new= session.new
        session_permanent=session.permanent


        #session = LocalProxy(partial(_lookup_req_object, 'session'))


        flask.flash('Logged in successfully.')

        next = flask.request.args.get('next')
        # next_is_valid should check if the user has valid
        # permission to access the `next` url
        #if not next_is_valid(next):
        #    return flask.abort(400)

        return flask.redirect(next or flask.url_for('index'))
    return flask.render_template('signin.html', form=form)



@mod_auth.route("/logout")
@login_required
def logout():
    logout_user()
    return flask.redirect(flask.url_for('index'))
