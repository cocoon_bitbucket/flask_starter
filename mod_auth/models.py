# flask wtf
import flask_wtf as wtf
import wtforms
from wtforms.validators import DataRequired

from model import Model

from engines import Engine

class User(Model):
    """

    """
    collection= 'Users'

    @property
    def is_authenticated(self):
        """
           Returns True if the user is authenticated,
           i.e. they have provided valid credentials.
           (Only authenticated users will fulfill the criteria of login_required.)

        :return:
        """
        return True

    @property
    def is_active(self):
        """
            Returns True if this is an active user - in addition to being authenticated,
            they also have activated their account, not been suspended, or any condition your application has
            for rejecting an account. Inactive accounts may not log in (without being forced of course).


        :return:
        """
        return True

    @property
    def is_anonymous(self):
        """
        Returns True if this is an anonymous user. (Actual users should return False instead.)

        """
        return False

    def get_id(self):
        """
            Returns a unicode that uniquely identifies this user,
            and can be used to load the user from the user_loader callback.

            Note that this must be a unicode - if the ID is natively an int or some other type,
            you will need to convert it to unicode.

        :return:
        """
        return self.data['name']


    # @classmethod
    # def get(cls,username):
    #     """
    #
    #     :param user_id:
    #     :return:
    #     """
    #     backend= Engine.get_instance()
    #
    #
    #     user=cls()
    #     user.username=username
    #     user.id= username
    #     return user


class LoginForm(wtf.Form):
    """

    """
    username = wtforms.StringField('name', validators=[DataRequired()])
    password = wtforms.PasswordField('password', validators=[DataRequired()])
