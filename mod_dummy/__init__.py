import flask
from flask import Blueprint,render_template

# flask login
from flask_login import login_required,current_user

from engines import Engine
from models import Dummies , DummiesForm, DummyEditForm


mod_dummy = Blueprint('dummy', __name__, template_folder="templates")

#from views import *




# @mod_dummy.route("/dummy")
# @login_required
# def dummy():
#     return render_template('dummy.html')
#     #return flask.redirect(flask.url_for('index'))


@mod_dummy.route('/dummies' ,methods=['GET', 'POST'])
@login_required
def dummies():
    """
        create a task
    :return:
    """
    backend= Engine.get_instance()


    form = DummiesForm()
    #form.target_type.choices=['MAC','fti']


    if form.validate_on_submit():
        # Login and validate the user.
        # user should be an instance of your `User` class

        #backend= flask.current_app.config['backend']
        #session=Sessions.get_by_token(access_token,backend)
        user=  current_user


        data= {
            'user_id': user.get_id(),
            'name': form.name.data,
            'message': form.message.data,
        }

        dummy= Dummies(_id=data['name'], data=data)
        dummy.save()


        flask.flash('Dummy created: %s ' % dummy._id)


        #return flask.redirect(flask.url_for('index'))
        return flask.redirect('/')


    return flask.render_template('dummy_new.html', form=form,header="Dummy")


@mod_dummy.route('/dummy_list' ,methods=['GET'])
def dummy_list():
    """
        create a task
    :return:
    """
    backend= Engine.get_instance()


    #item_list= Dummies(backend)
    dummies= Dummies.objects().all()


    return flask.render_template('dummy_list.html',items=dummies,header='Dummy')

@mod_dummy.route('/dummy/<string:item_id>' ,methods=['GET', 'POST'])
@login_required
def dummy_edit(item_id):
    """
        create a task
    :return:
    """
    backend= Engine.get_instance()
    form = DummyEditForm()

    if form.validate_on_submit():

        # modify dummy
        dummy = Dummies.get(item_id)
        dummy.data['message']= form.message.data
        dummy.save()

        flask.flash('Dummy modified: %s ' % dummy._id)

        # return flask.redirect(flask.url_for('index'))
        return flask.redirect('/dummy_list')

    else:
        # edit item
        dummy= Dummies.get(item_id)


        form.name.data= dummy.attribute('name')
        form.message.data= dummy.attribute('message')
        form.user_id.data = dummy.attribute('user_id')



        return flask.render_template('dummy_edit.html',form=form,header='Dummy')