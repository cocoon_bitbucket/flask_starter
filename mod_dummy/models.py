# flask wtf
import flask_wtf as wtf
import wtforms
import wtforms_components

from wtforms.validators import DataRequired

from model import Model

from engines import Engine



class Dummies(Model):
    """

    """
    collection= 'dummies'


    # @classmethod
    # def get_by_name(cls,name):
    #     """
    #
    #     :param user_id:
    #     :return:
    #     """
    #     backend= Engine.get_instance()
    #
    #
    #     obj=cls()
    #     obj.name = name
    #     obj.message = "dummy"
    #     return obj


class DummiesForm(wtf.Form):
    """

    """
    name = wtforms.StringField('name', validators=[DataRequired()])
    message = wtforms.StringField('message', validators=[DataRequired()])


class DummyEditForm(wtf.Form):
    """

    """
    name = wtforms.StringField('name')
    user_id= wtforms.StringField('owner')
    message = wtforms.StringField('message')



    def __init__(self, *args, **kwargs):
        super(DummyEditForm, self).__init__(*args, **kwargs)
        # make name and user_id non editable
        wtforms_components.read_only(self.name)
        wtforms_components.read_only(self.user_id)
