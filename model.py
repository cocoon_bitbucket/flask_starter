# flask wtf
import flask_wtf as wtf
import wtforms
from wtforms.validators import DataRequired

# database
from engines import Engine



class Model(object):
    """



    """
    collection= "Items"

    def __init__(self,_id,data):
        """

        :param _id:
        :param data:
        """
        self._id=_id
        self.data=data


    @classmethod
    def get(cls,_id,backend=None):
        """

        :param _id:
        :return:
        """
        backend=backend or Engine.get_instance()
        key= cls._key(_id)
        item_data= backend.get(key)
        if item_data is not None:
            return cls(_id,item_data)
        return None


    @classmethod
    def _key(cls,_id):
        """

        :param _id:
        :return:
        """
        return "%s:%s" % (cls.collection,_id)

    @classmethod
    def _key_parts(cls,key):
        if ":" in key:
            return key.split(":",-1)
        return "",key

    @property
    def backend(self):
        return Engine.get_instance()

    def save(self):
        """

        :param backend:
        :return:
        """
        backend=self.backend
        key= self._key(self._id)
        self.data['_id']= self._id
        backend.set(key,self.data)


    def attribute(self,name):
        """

        :param name:
        :return:
        """
        return self.data[name]

    @classmethod
    def objects(cls):
        """

        :return: a query
        """
        return Query(cls,source=None)

    query=objects





class Query(object):
    """

        chainable query

    :param object:
    :return:
    """
    def __init__(self, type,source=None,backend=None):
        """

        :param type: Model class or derived
        :param source: a generator
        :param backend:
        :return:
        """
        self.backend = backend or Engine.get_instance()
        self.type= type

        # source or item iterator
        source= source or self._iter_items(self._iter_id())

        self.source= source

    def _split_key(self, key):
        """

        :param key: string  eg Users:1
        :return: a tuple ( collection, _id )
        """
        if ":" in key:
            return key.split(":", -1)
        else:
            return "", key


    def _iter_id(self):
        """
            generator : all keys for the model

        """
        # keys= self.session.keys()

        for key in self.backend.keys():
            if key.startswith("%s:" % self.type.collection):
                col, _id = self._split_key(key)
                yield _id


    def _iter_items(self, source):
        """
            iterate Model instances
        :param source: generator
        :return:
        """
        for _id in source:
            yield self.type.get(_id)


    def _filter(self, source, filter):
        """

        :param source: a generator
        :param filter: function returning True or False
        :return:
        """
        for item in source:
            r = filter(item)
            if r:
                yield item


    #
    # interface
    #
    #def run(self):
    #    return self.source

    def one(self):
        """

        :return:
        """
        for item in self.source:
            return item

    def all(self):
        return list(self.source)


    @property
    def items(self):
        """

        :return: a new query
        """
        source= self._iter_id()
        source= self._iter_items(source)
        next= Query(self.type,source,self.backend)
        return next

    def filter(self,filter):
        """

        :param filter:
        :return: a new query
        """
        assert self.source is not None
        source= self._filter(self.source,filter)
        next= Query(self.type,source,self.backend)
        return next




if __name__=="__main__":


    class Items(Model):
        collection= "Items"


    bk= Engine.new()

    i1= Items('1',{ 'name': 'item1'})
    i1.save()

    i2 = Items('2', {'name': 'item2'})
    i2.save()


    i1= Items.get('1')
    assert i1.data['name'] == 'item1'

    i0= Model.get('0')
    assert i0 == None

    filter= lambda item: item.attribute('name') == 'item2'

    items= Items.objects().filter(filter).all()
    assert items[0].data['name'] == 'item2'

    print list(items)



    print "Done."